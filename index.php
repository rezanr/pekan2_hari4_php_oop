<?php



	require_once('animal.php');
	require_once('frog.php');
	require_once('ape.php');

	// Class Shaun
	echo "RELEASE 0 <br>";
	$sheep = new animal ("Shaun The Sheep");

	echo "Nama hewan : " . $sheep->name . "<br>";
	echo "Jumlah kaki : " . $sheep->legs . "<br>";
	echo "Hewan berdarah dingin : " . $sheep->cold_blooded . "<br><br>";
	// Akhir Class Shaun

	// Class Kodok
	echo "RELEASE 1 <br>";

	$kodok = new frog ("Bulfrog");

	echo "Nama hewan : " . $kodok->name . "<br>";
	echo "Jumlah kaki : " . $kodok->legs . "<br>";
	echo "Hewan berdarah dingin : " . $kodok->cold_blooded . "<br>";
	echo "Suara khas memanggil : " . $kodok->jump . "<br><br>" ; // "hop hop"
	// AKhir Kodok


	// Class Ape
	$sungokong = new ape ("Kera sakti");

	echo "Nama hewan : " . $sungokong->name . "<br>";
	echo "Jumlah kaki : " . $sungokong->legs . "<br>";
	echo "Hewan berdarah dingin : " . $sungokong->cold_blooded . "<br>";
	echo "Suara khas memanggil : " . $sungokong->yell . "<br>" ; // "hop hop"

	// Akhir Ape
?>